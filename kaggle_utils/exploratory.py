from collections import defaultdict
import matplotlib.pyplot as plt
import numpy as np
import os


def get_numeric_cols(df):
    numeric_cols = [col for col in df.columns if df[col].dtype.name in ['int64', 'float64']]
    return df[numeric_cols]


def _make_num_var_plots(col, outcome_row_nums, col_name, out_dir):
    label_values_dict = _make_label_values_dict(col, outcome_row_nums)

    num_plots = len(label_values_dict) + 1

    fig, axes = plt.subplots(nrows=1, ncols=num_plots, figsize=(8*num_plots, 8))

    axes[0].hist(col, bins=20)

    for i, (label, vals) in enumerate(label_values_dict.iteritems()):
        axes[i+1].boxplot(vals, labels=[label])  # TODO normalise y axis

    plt.savefig(os.path.join(out_dir, col_name+'_charts.png'))
    plt.cla()


def _get_outcome_row_nums(labels):
    """
    get the indices of all of the occurrences of each unique label and return them in a dict
    :param labels: pandas series or a list
    :return: a dictionary relating each label with a list of indices, i.e. {label_name: [i0, i1, i2,...]}
    """
    outcome_row_nums = []

    for i, label in enumerate(labels):
        outcome_row_nums.append((label, i))

    outcome_dict = defaultdict(list)
    for k, v in outcome_row_nums:
        outcome_dict[k].append(v)

    return outcome_dict


def _make_label_values_dict(col, outcome_row_nums):
    """
    subset a feature column by outcome and return in a dict
    :param col: pandas series or list or np array
    :param outcome_row_nums: dictionary -- {label: [x0, x1, x2, ...]} where xi are row nums corresponding to label
    :return: a dictionary in the form {label_name: [x0, x1, x2, ...]} where xi are col values corresponding to the label
    """
    label_values_dict = {}
    for label, row_nums in outcome_row_nums.iteritems():
        col_select = col[row_nums]
        label_values_dict[label] = col_select
    return label_values_dict


def generate_numeric_var_summary(df, labels, out_dir):
    summary_file = open(os.path.join(out_dir, 'num_var_summary.txt'), 'w')
    numeric_df = get_numeric_cols(df)
    summary = str(numeric_df.describe())
    summary_file.write(summary)
    summary_file.close()
    print summary

    outcome_row_nums = _get_outcome_row_nums(labels)

    for col_name in numeric_df.columns:
        # make the following into a _make_num_var_plots function given col, labels, out_dir
        col = numeric_df[col_name].values
        _make_num_var_plots(col, outcome_row_nums, col_name, out_dir)


def count_outcomes(labels):
    """
    given a series/array/list of labels, count the number of occurrences of each unique label
    :param labels: series/array/list of labels
    :return: a dictionary -- {label1: count1, label2: count2, ...}
    """
    outcome_counts = defaultdict(int)
    for label in labels:
        outcome_counts[label] += 1
    return outcome_counts


def count_class_outcomes(col, vals, labels):
    """
    for each value in a list, count the number of occurrences of each label for observations where the value occurs
    :param col: a variable column in a dataframe, pandas series
    :param vals: a list of values to query
    :param labels: the label column of a dataframe (class variable)
    :return: dict of count_outcomes (see above) -- {value1: count_outcomes1, value2: count_outcomes2, ...}
    """
    col_list = list(col)
    labels_list = list(labels)
    class_outcomes = {}

    for val in vals:
        indices = [i for i, v in enumerate(col_list) if v == val]
        val_labels = [label for i, label in enumerate(labels_list) if i in indices]
        outcomes = count_outcomes(val_labels)
        class_outcomes[val] = outcomes

    return class_outcomes


def class_variable_counts(col):
    """
    count the number of instances of each class in a column
    :param col: pandas series/numpy array/list
    :return: a dictionary -- {label1: label1_count, ...}
    """
    unique_val_counts = defaultdict(int)

    for item in col:
        unique_val_counts[item] += 1

    return unique_val_counts


def _summarise_class_var(col, n_list, labels):
    class_counts = class_variable_counts(col)

    no_classes = len(class_counts)
    no_missing = class_counts[np.nan]
    sorted_counts = sorted(class_counts.iteritems(), key=lambda (k, v): v, reverse=True)
    most_common = sorted_counts[:min(n_list, no_classes)]

    summary = {
        'no_classes': no_classes,
        'no_missing': no_missing,
        'most_common': most_common,
        'outcomes': count_class_outcomes(col, [mc[0] for mc in most_common], labels)
    }
    return summary


def _print_class_var_summary(variable_name, summary):  # TODO: return a string, then put in .md format -- easy to print and write to file
    print "\n{}: ".format(variable_name)

    print "\t{} classes".format(summary['no_classes'])

    print "\t{} values missing".format(summary['no_missing'])

    print "\tmost common values:"

    for value, count in summary['most_common']:
        print "\t\t{0} ({1} instances)".format(value, count)
        print "\t\t\toutcomes:"
        for k, v in summary['outcomes'][value].iteritems():
            print "\t\t\t\t{0}: {1}%".format(k, 100.0*v/count)


def generate_class_variables_summary(df, labels):
    summary = {}

    for column in df.columns:
        column_data = df[column]
        if column_data.dtype.name == 'object':
            col_summary = _summarise_class_var(df[column], 10, labels)
            _print_class_var_summary(column, col_summary)
            summary[column] = col_summary
    return summary
