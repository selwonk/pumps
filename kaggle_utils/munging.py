import numpy as np
from datetime import datetime
from copy import deepcopy
import exploratory as explore


def recast_class_variable(column):
    """
    recasts a class variable from string to integer
    :param column: a pandas data series
    :return: the input data series in integer form
    """
    unique_vals = set(column)
    data_dict = {}
    for i, val in enumerate(unique_vals):
        data_dict[val] = i
    recast_column = column.map(data_dict).astype(int)
    print
    return recast_column


def recast_all_class_variables(df, var_types={}):
    """
    iterate through the columns of a dataframe and recast any class variables to integers
    :param df: dataframe
    :return: dataframe with all numerical variable
    """
    new_df = deepcopy(df)

    for column in df.columns:
        column_data = df[column]
        if column_data.dtype.name == 'object':
            new_df[column] = recast_class_variable(df[column])

    return new_df


def datestrings_to_ints(datestrings, date_format, ref_date=None):
    """
    convert datestrings to integers representing days in relation to a reference date
    :param datestrings: a pandas sequence of datestrings
    :param date_format: the datetime format of the datestrings
    :param ref_date:
    :return: a sequence of integers
    """
    dates = [datetime.strptime(datestring, date_format) for datestring in datestrings]
    sorted_unique_dates = sorted(set(dates))
    if not ref_date:
        ref_date = sorted_unique_dates[-1] + (sorted_unique_dates[-1] - sorted_unique_dates[-2])
    days = dates_to_ints(dates, ref_date)
    return days


def dates_to_ints(dates, ref_date):
    """
    convert a sequence of dates into integers representing days in relation to a reference date
    :param dates: a sequence of datetimes
    :param ref_date: a reference datetime
    :return: a sequence of integers corresponding to days from reference date
    """
    days = [(ref_date - date).days for date in dates]
    return days


def encode_class_variable(col, mapping):
    """
    encode a class variable using a binary matrix
    :param col: a sequence of strings representing a class variable
    :param mapping: a dictionary mapping each possible value to an integer
    :return: a binary matrix where each column represents a column and each row consists of all 0s and a single 1
    """
    no_classes = len(set(mapping.values()))

    encoding = np.zeros((len(col), no_classes), dtype=np.int)

    for i in xrange(len(col)):
        val = col[i]
        encoding[i, mapping[val]] = 1

    return encoding


def class_variable_mapping(col, n=None):
    """
    create a mapping from each value of a class variable to a corresponding integer in order of frequency
    :param col: a sequence of values representing a class variable
    :param n: n+1 is the max # of classes in the new mapping --> the n most common classes and the rest as an 'other' class
    :return: a dictionary mapping each value of the original sequence to an integer [0, n]
    """
    class_counts = explore.class_variable_counts(col)
    sorted_classes = sorted(class_counts.iteritems(), key=lambda (value, count): count, reverse=True)
    num_classes = len(sorted_classes)

    if n is None:
        n = num_classes

    mapping = {}

    if n < num_classes:
        for i in xrange(n):
            mapping[sorted_classes[i][0]] = i
        for val in sorted_classes[n:]:
            mapping[val[0]] = n
    else:
        for i, val in enumerate(sorted_classes):
            mapping[val[0]] = i

    return mapping


def encode_dataframe(df, class_var_limit=10):
    encoded_array = None

    for column in df.columns:
        column_data = df[column]
        if column_data.dtype.name == 'object':
            mapping = class_variable_mapping(column_data, class_var_limit)
            encoded_col = encode_class_variable(column_data, mapping)
        else:
            encoded_col = column_data.values.reshape((-1, 1))

        if encoded_array is None:
            encoded_array = encoded_col
        else:
            encoded_array = np.concatenate((encoded_array, encoded_col), axis=1)

    return encoded_array


if __name__ == "__main__":
    import pandas as pd
    df = pd.read_csv('/media/sf_personal/pumps/raw_data/pumps_training_data.csv', header=0)
    mapping = class_variable_mapping(df['permit'])
    test = encode_class_variable(df['permit'], mapping)
    print test.shape
