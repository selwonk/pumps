import numpy as np
import itertools as it
from random import shuffle


def kfold_cross_val(X, y, k, model_class, **params):
    """
    perform k-fold cross-validation of a model with a set of params on a data set and compute the avg accuracy
    :param X: data stored in a numpy array
    :param y: labels stored in a numpy array
    :param k: integer -- the number of folds
    :param model_class: this should be a sklearn model class
    :param params: a set of parameters to use for the model
    :return: the average score
    """
    num_rows, num_cols = X.shape
    scores = []

    deck = range(num_rows)
    shuffle(deck)

    fold_indices = [(i)*num_rows/k for i in xrange(k+1)]

    for i in xrange(k):
        print "\tfold{}:".format(i+1),
        xval_indices = deck[fold_indices[i]:fold_indices[i+1]]
        training_indices = list(set(deck) - set(xval_indices))
        training_folds = X[training_indices]
        training_labels = y[training_indices]
        xval_fold = X[xval_indices]
        xval_labels = y[xval_indices]

        model = model_class(**params)
        model.fit(training_folds, training_labels)
        score = (model.score(xval_fold, xval_labels))
        scores.append(score)
        print score

    mean_score = np.array(scores).mean()
    print "mean score: {}".format(mean_score)
    return mean_score


def repeated_kfold_cross_val(X, y, k, repeats, model_class, **params):
    """
    perform repeated k-fold cross-validation
    :param X: data -- numpy array
    :param y: labels -- numpy array
    :param k: # of folds -- integer
    :param repeats: # of repeats -- integer
    :param model_class: sklearn model
    :param params: model parameters
    :return: mean score
    """
    xval_scores = []
    for repeat in xrange(repeats):
        print "\nRepeat{}:".format(repeat+1)
        xval_scores.append(kfold_cross_val(X, y, k, model_class, **params))
    final_score = np.array(xval_scores).mean()
    print "\nFinal score: {}".format(final_score)
    return final_score


def parse_parameter_set(param_set):
    """
    convert param_set from {param1: [list of values], param2:{list of values} ...} to a list with all of the possible
    permutations of the parameters s.t. each item of the list is of the form [{param1: val, param2: val, ...} ...]
    :param param_set: a dictionary of the form {"name_of_param": [list of vals to test], ... }
    :return: a list of dictionaries containing all of the permutations of parameter values
    """
    param_permutations = [dict(zip(param_set, prod)) for prod in it.product(*(param_set[key] for key in param_set.keys()))]
    return param_permutations


def test_parameters(X, y, k, repeats, model_class, param_set):
    """
    find out which set of parameters of a model scores best on a data set using repeated k-fold cross-validation
    :param X: data -- numpy array
    :param y: labels -- numpy array
    :param k: # of folds -- integer
    :param repeats: # of repeats -- integer
    :param model_class: sklearn model class
    :param param_set: a list of dictionaries representing sets of parameters to test
    :return: the best set of parameters and their mean score
    """
    best_score = None
    best_params = None

    for params in param_set:
        print "\nTesting params: {}".format(params)
        model_score = repeated_kfold_cross_val(X, y, k, repeats, model_class, **params)
        if best_score is None:
            best_score = model_score
            best_params = params
        elif model_score > best_score:
            best_score = model_score
            best_params = params

    print "\nResults of {0}x {1}-fold cross-validation:".format(repeats, k)
    print "The best params were: {}".format(best_params)
    print "The best score was : {}".format(best_score)

    return best_params, best_score
