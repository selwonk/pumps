import pandas as pd
import csv


def make_submission(test_data, munge_function, final_model, label_mapping, id_field_name, label_field_name, outfile):
    """
    Make a submission file, given test data, a munging function, a final model, field names, and a location to write
    :param test_data: a pandas dataframe of raw test data
    :param munge_function: a function used to preprocess data
    :param final_model: a sklearn model to use to make predictions
    :param id_field_name: a string
    :param label_field_name: a string
    :param outfile: location and filename to write submission file
    """
    # read in test data and put aside IDs
    id_list = test_data[id_field_name]

    # munge test_data
    test_data = munge_function(test_data)

    # get predictions
    predictions = pd.DataFrame(final_model.predict(test_data))
    predictions = predictions[0].map(label_mapping)

    with open(outfile, 'wb') as submission_file:
        wr = csv.DictWriter(submission_file, fieldnames=[id_field_name, label_field_name])
        wr.writeheader()
        rows = [{id_field_name: i, label_field_name: prediction} for i, prediction in zip(id_list, predictions)]
        for row in rows:
            wr.writerow(row)

