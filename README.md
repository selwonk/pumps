# Pump It Up #

This is my attempt at the [Pump it Up project at drivendata.org](http://www.drivendata.org/competitions/7/)

The purpose of this project is to be able to predict whether pumps in Tanzania are functional, need repairs, or completely broken. An accurate model will hopefully improve the efficiency of maintenance operations and bring more potable water to more communities across Tanzania.