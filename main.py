import frigg.munging as munging
import frigg.model_building as modelling
import frigg.submission as submission
from sklearn import ensemble
import pandas as pd
import pickle
import gzip


def munge_pumps_data(X):
    drop_list = ['id',
                 'date_recorded',
                 'funder',
                 'installer',
                 'wpt_name',
                 'subvillage',
                 'ward',
                 'recorded_by',
                 'scheme_name']

    # convert date_recorded to days
    X['days'] = munging.datestrings_to_ints(X['date_recorded'], "%Y-%m-%d")

    # drop unwanted_cols
    X = X.drop(drop_list, axis=1)

    # X = munging.recast_all_class_variables(X)
    X = munging.encode_dataframe(X, 10)

    # return X.values

    return X


def main():
    data = pd.read_csv('raw_data/pumps_training_data.csv', header=0)

    X = munge_pumps_data(data)

    print X.shape

    labels = pd.read_csv('raw_data/pumps_training_labels.csv', header=0)
    y = labels['status_group'].map({'non functional': 0,
                                    'functional needs repair': 1,
                                    'functional': 2}).astype(int).values

    model_params = {
        "n_estimators": [500],
        "max_features": [20, 40, 60]
    }

    param_set = modelling.parse_parameter_set(model_params)

    best_params, best_score = modelling.test_parameters(X, y, k=5, repeats=3,
                                                        model_class=ensemble.RandomForestClassifier,
                                                        param_set=param_set)

    print "\nBuilding final model"
    best_params['n_estimators'] = 2500
    best_params['oob_score'] = True
    final_model = ensemble.RandomForestClassifier(**best_params)
    final_model.fit(X, y)
    print "OOB score of final model: {}".format(final_model.oob_score_)

    test_data = pd.read_csv('raw_data/pumps_test_data.csv', header=0)

    label_mapping = {
        0: 'non functional',
        1: 'functional needs repair',
        2: 'functional'
    }

    print '\nMaking submission file'
    submission_params = {
        'test_data': test_data,
        'munge_function': munge_pumps_data,
        'final_model': final_model,
        'label_mapping': label_mapping,
        'id_field_name': 'id',
        'label_field_name': 'status_group',
        'outfile': 'submissions/submission1.csv'
    }
    submission.make_submission(**submission_params)


if __name__ == "__main__":
    main()
